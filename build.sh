#!/bin/bash

clang++ \
    -Os \
    --no-standard-libraries \
    -target wasm32 \
    -Wl,--no-entry \
    -Wl,--export-all \
    -Wl,--allow-undefined \
    strs.cpp \
    -o strs.wasm
