extern "C" {

const char *str_ret() { return "string returned from C++"; }

unsigned str_len(const char *str) {
  const char *ptr = str;
  while (ptr != 0 && *ptr != '\0') {
    ptr++;
  }
  return ptr - str;
}

unsigned count_digits(const char *str) {
  unsigned digits = 0;
  while (str != 0 && *str != '\0') {
    if (*str >= '0' && *str <= '9') {
      digits++;
    }
    str++;
  }
  return digits;
}
}
