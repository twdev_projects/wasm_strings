const importObject = {};

function importWasmStr(wasmMem, strPtr, strLen) {
  let view = new DataView(wasmMem.buffer, strPtr, strLen)
  let dec = new TextDecoder();
  return dec.decode(view);
}

function exportJsStr(wasmMem, wasmStrPtr, jsStr) {
  let strLen = jsStr.length + 1;
  let strArr = new Uint8Array(wasmMem.buffer, wasmStrPtr, strLen);
  let enc = new TextEncoder();
  enc.encodeInto(jsStr, strArr);
}

WebAssembly.instantiateStreaming(fetch("strs.wasm"), importObject)
    .then((wasm) => {
      console.log(wasm.instance.exports);
      const {str_ret, str_len, count_digits} = wasm.instance.exports;
      const mem = wasm.instance.exports.memory;

      const wasmStr = str_ret();
      const wasmStrLen = str_len(wasmStr);

      console.log(importWasmStr(mem, wasmStr, wasmStrLen));

      let jsStr = "This string contains some 123 digits";
      const pageSize = 64 * 1024;
      let wasmExtrMemPtr = mem.grow(1) * pageSize;
      exportJsStr(mem, wasmExtrMemPtr, jsStr);

      console.log(count_digits(wasmExtrMemPtr));
    });
